package pw.nerde.riichala.test

import pw.nerde.riichala._
import java.io.File
import scala.xml.XML.loadFile
import scala.xml.Node

object TestTenhou {
  val Kaze: Seq[Kazehai] = Seq(Ton, Nan, Sha, Pei)

  def listDir(dir: String): List[File] = {
    val d = new File(dir)
    if (d.exists && d.isDirectory)
      d.listFiles.filter(_.isFile).toList
    else
      List[File]()
  }

  def mkTile(i: Int): Tile = i match {
    case x if x < 36              => Manzu(x / 4 + 1)
    case x if 36 <= x && x < 72   => Pinzu((x - 36) / 4 + 1)
    case x if 72 <= x && x < 108  => Souzu((x - 72) / 4 + 1)
    case x if 108 <= x && x < 124 => Kaze((x - 108) / 4)
    case x if 124 <= x && x < 128 => Haku
    case x if 128 <= x && x < 132 => Hatsu
    case x if 132 <= x            => Chun
  }

  def specialYaku(i: Int): Option[Yaku.SpecialYaku] = {
    if (i == 1) Some(Yaku.Riichi)
    else if (i == 2) Some(Yaku.Ippatsu)
    else if (i == 3) Some(Yaku.Chankan)
    else if (i == 4) Some(Yaku.Rinshan)
    else if (i == 5) Some(Yaku.Haitei)
    else if (i == 6) Some(Yaku.Houtei)
    else if (i == 21) Some(Yaku.DoubleRiichi)
    else if (i == 36) Some(Yaku.Renhou)
    else if (i == 37) Some(Yaku.Tenhou)
    else if (i == 38) Some(Yaku.Chiihou)
    else None
  }

  def commaList(str: String): Seq[Int] =
    if (str.isEmpty) Seq.empty else str.split(',').map(_.toInt)

  def countAka(ids: Seq[Int]): Int =
    ids.count(x => x == 16 || x == 52 || x == 88)

  // References:
  // https://github.com/MahjongRepository/tenhou-python-bot/blob/master/project/tenhou/decoder.py
  // https://github.com/MahjongRepository/tenhou-log
  def mkMentsu(i: Int): (Mentsu, Int) = {
    if ((i & 0x4) != 0) {
      val _tiles = Seq((i >> 3) & 0x3, (i >> 5) & 0x3, (i >> 7) & 0x3)
      val baseAndCall = i >> 10
      val _base = baseAndCall / 3
      val base = (_base / 7) * 9 + (_base % 7)
      val ids = _tiles.zipWithIndex.map(x => x._1 + 4 * (base + x._2))
      val tiles = ids.map(x => mkTile(x)).collect { case s: Shuupai => s }
      (Shuntsu(tiles), countAka(ids))
    } else if ((i & 0x18) != 0) {
      val t4 = (i >> 5) & 0x3
      val t = Seq(0, 1, 2, 3).patch(t4, Nil, 1)
      val baseAndCall = i >> 9
      val base = baseAndCall / 3
      val ids = t.map(_ + 4 * base)
      val tiles = ids.map(x => mkTile(x))
      if ((i & 0x8) != 0) {
        (Koutsu(tiles), countAka(ids))
      } else {
        val chakan = t4 + 4 * base
        (Kantsu(mkTile(chakan) +: tiles), countAka(chakan +: ids))
      }
    } else {
      val baseAndCall = i >> 8
      val base = baseAndCall / 4
      val ids = (0 to 3).map(_ + 4 * base)
      val tiles = ids.map(x => mkTile(x))
      val ankan = (i & 0x3) == 0
      (Kantsu(tiles, ankan), countAka(ids))
    }
  }

  case class HandInfo(
      winningHand: WinningHand,
      winds: Seq[Kazehai],
      doras: Seq[Tile],
      ansScore: Int)

  def mkHand(node: (Node, Node)): HandInfo = {
    val round = commaList(node._1 \@ "seed")(0)
    val who = node._2 \@ "who"
    val tsumo = who == (node._2 \@ "fromWho")
    val isOya = (node._2 \@ "who") == (node._1 \@ "oya")
    val agaripai = mkTile((node._2 \@ "machi").toInt)
    val haiIds = commaList(node._2 \@ "hai")
    val hai = haiIds.map(x => mkTile(x))
    val fuuro = commaList(node._2 \@ "m").map(s => mkMentsu(s))
    val yaku =
      (commaList(node._2 \@ "yaku") ++ commaList(node._2 \@ "yakuman"))
        .grouped(2)
        .flatMap(s => specialYaku(s.head))
        .toSeq
    val doras = commaList(node._2 \@ "doraHai").map(s => mkTile(s))
    val uraDoras = commaList(node._2 \@ "doraHaiUra").map(s => mkTile(s))
    val allDoras =
      if (yaku.contains(Yaku.Riichi) || yaku.contains(Yaku.DoubleRiichi))
        doras ++ uraDoras
      else
        doras
    val aka = fuuro.map(_._2).sum + countAka(haiIds)
    val winningHand =
      WinningHand(Hand(hai, fuuro.map(_._1)), tsumo, isOya, agaripai, yaku, aka)
    val winds = Seq(Kaze(round / 4), Kaze((who.toInt - round % 4 + 4) % 4))
    val ansScore = commaList(node._2 \@ "ten")(1).toInt
    HandInfo(winningHand, winds, allDoras, ansScore)
  }

  def testHand(info: HandInfo): Option[ScoreCalculator.Result] = {
    val result = ScoreCalculator(info.winningHand, info.winds, info.doras)
    if (result.cost.total != info.ansScore) Some(result) else None
  }

  def nodeName(node: Node): String =
    node.nameToString(new StringBuilder).toString

  def main(args: Array[String]) {
    val t0 = System.nanoTime()
    val nodeList = listDir("logs/").toStream.flatMap { f =>
      loadFile(f).descendant.filter { node =>
        Seq("INIT", "AGARI", "RYUUKYOKU").contains(nodeName(node))
      }
    }
    val handList = nodeList
      .scanLeft(null: Node) {
        case (acc, cur) => if (nodeName(cur) == "INIT") cur else acc
      }
      .tail
      .zip(nodeList)
      .filter(x => nodeName(x._2) == "AGARI")
      .map(x => (x, mkHand(x)))
      .toList
    println("Parse time: " + (System.nanoTime() - t0) + "ns")

    val t1 = System.nanoTime()
    val resList = handList.par.map(t => (t._1, t._2, testHand(t._2)))
    resList.find(_._3.isDefined) match {
      case Some(r) =>
        println("Error Found!")
        println(r)
      case None =>
        println("OK!")
        println("Scanned: " + resList.size)
    }
    println("Scoring time: " + (System.nanoTime() - t1) + "ns")
  }
}
