import com.thoughtworks.binding.Binding.{Constants, Var, Vars}
import com.thoughtworks.binding.{Binding, dom}
import org.scalajs.dom.raw.Event
import org.scalajs.dom._
import org.scalajs.dom.ext._
import pw.nerde.riichala._

object Adapter {
  val Kaze: Map[String, Kazehai] =
    Map("east" -> Ton, "south" -> Nan, "west" -> Sha, "north" -> Pei)
  val SpecialYaku = Map(
    "riichi" -> Yaku.Riichi,
    "doubleriichi" -> Yaku.DoubleRiichi,
    "ippatsu" -> Yaku.Ippatsu,
    "haitei" -> Yaku.Haitei,
    "rinshan" -> Yaku.Rinshan,
    "chankan" -> Yaku.Chankan,
    "tenhou" -> Yaku.Tenhou)

  def mkTile(tileType: Char, i: Int): Tile = tileType match {
    case c if c == 'm' && 1 <= i && i <= 9 => Manzu(i)
    case c if c == 'p' && 1 <= i && i <= 9 => Pinzu(i)
    case c if c == 's' && 1 <= i && i <= 9 => Souzu(i)
    case c if c == 'z' && i == 1           => Ton
    case c if c == 'z' && i == 2           => Nan
    case c if c == 'z' && i == 3           => Sha
    case c if c == 'z' && i == 4           => Pei
    case c if c == 'z' && i == 5           => Haku
    case c if c == 'z' && i == 6           => Hatsu
    case c if c == 'z' && i == 7           => Chun
  }

  def mkHand(str: String): (Seq[Tile], Seq[Mentsu]) = {
    val (emTile, emMend) = (List.empty[Tile], List.empty[Mentsu])
    val res = str.reverse.foldLeft(('z', emTile, emTile, emMend)) {
      case ((tileType, mentsuAcc, handAcc, fuuroAcc), cur) =>
        cur match {
          case c if Seq('m', 'p', 's', 'z') contains c =>
            (c, mentsuAcc, handAcc, fuuroAcc)
          case i if '1' to '9' contains i =>
            val tile = mkTile(tileType, i - '0')
            (tileType, tile :: mentsuAcc, handAcc, fuuroAcc)
          case ')' | ']' =>
            (tileType, List.empty, mentsuAcc ++ handAcc, fuuroAcc)
          case '(' =>
            val newMentsu = HandElement.from(mentsuAcc.sorted).asInstanceOf[Mentsu]
            (tileType, List.empty, handAcc, newMentsu +: fuuroAcc)
          case '[' =>
            val newMentsu = Kantsu(mentsuAcc.sorted, true)
            (tileType, List.empty, handAcc, newMentsu +: fuuroAcc)
          case _ =>
            (tileType, mentsuAcc, handAcc, fuuroAcc)
        }
    }
    (res._2 ++ res._3, res._4)
  }

  def apply(
      handStr: String,
      agariStr: String,
      akadora: String,
      doraStr: String,
      tsumoStr: String,
      seatStr: String,
      roundStr: String,
      special: Seq[String]): ScoreCalculator.Result = {
    try {
      val (handTuple, doras) = (mkHand(handStr), mkHand(doraStr)._1)
      val kaze = Seq(Kaze(seatStr), Kaze(roundStr))
      val tsumo = tsumoStr == "tsumo"
      val hand = Hand(handTuple._1, handTuple._2)
      val oya = seatStr == "east"
      val agaripai = mkTile(agariStr(1), agariStr(0) - '0')
      val specialYaku = special.map(s => SpecialYaku(s))
      val winningHand =
        WinningHand(hand, tsumo, oya, agaripai, specialYaku, akadora.toInt)
      ScoreCalculator(winningHand, kaze, doras)
    } catch {
      case e: Exception =>
        ScoreCalculator.Result(
          0,
          NoLimit,
          ScoreCalculator.NoAgari,
          Seq.empty,
          Seq.empty)
    }
  }

  def resToStr(result: ScoreCalculator.Result): String = {
    val yakuCnt = result.yakus.map(_._2).sum
    val fuCnt = result.fus.map(_._2).sum
    val cost = result.cost match {
      case ScoreCalculator.OyaTsumo(a) => f"$a"
      case ScoreCalculator.Tsumo(a, b) => f"$a-$b"
      case ScoreCalculator.Ron(a)      => f"$a"
      case _                           => "Not winning hand"
    }
    f"${yakuCnt} Han, ${fuCnt} Fu, ${result.limit}, $cost"
  }
}

class Ui {
  val result: Vars[ScoreCalculator.Result] = Vars()

  @dom
  def form = {
    <div class="form-horizontal">
      <div class="form-group">
        <div class="col-3 col-sm-12">
          <label class="form-label">Hand</label>
        </div>
        <div class="col-9 col-sm-12">
          <input class="form-input" id="hand" type="text" />
        </div>
      </div>
      <div class="form-group">
        <div class="col-3 col-sm-12">
          <label class="form-label">Agaripai</label>
        </div>
        <div class="col-9 col-sm-12">
          <input class="form-input" id="agaripai" type="text" />
        </div>
      </div>
      <div class="form-group">
        <div class="col-3 col-sm-12">
          <label class="form-label">Akadoras</label>
        </div>
        <div class="col-9 col-sm-12">
          <input class="form-input" id="aka" type="number" />
        </div>
      </div>
      <div class="form-group">
        <div class="col-3 col-sm-12">
          <label class="form-label">Dora Indicators</label>
        </div>
        <div class="col-9 col-sm-12">
          <input class="form-input" id="dora" type="text" />
        </div>
      </div>
      <div class="form-group">
        <div class="col-3 col-sm-12">
          <label class="form-label">Win By</label>
        </div>
        <div class="col-9 col-sm-12">
          <label class="form-radio form-inline">
            <input name="tsumo" value="tsumo" type="radio" checked={true} />
            <i class="form-icon"></i>Tsumo
          </label>
          <label class="form-radio form-inline">
            <input name="tsumo" value="ron" type="radio" />
            <i class="form-icon"></i>Ron
          </label>
        </div>
      </div>
      <div class="form-group">
        <div class="col-3 col-sm-12">
          <label class="form-label">Seat Wind</label>
        </div>
        <div class="col-9 col-sm-12">
          <label class="form-radio form-inline">
            <input name="seat" value="east" type="radio" checked={true} />
            <i class="form-icon"></i>East
          </label>
          <label class="form-radio form-inline">
            <input name="seat" value="south" type="radio" />
            <i class="form-icon"></i>South
          </label>
          <label class="form-radio form-inline">
            <input name="seat" value="west" type="radio" />
            <i class="form-icon"></i>West
          </label>
          <label class="form-radio form-inline">
            <input name="seat" value="north" type="radio" />
            <i class="form-icon"></i>North
          </label>
        </div>
      </div>
      <div class="form-group">
        <div class="col-3 col-sm-12">
          <label class="form-label">Round Wind</label>
        </div>
        <div class="col-9 col-sm-12">
          <label class="form-radio form-inline">
            <input name="round" value="east" type="radio" checked={true} />
            <i class="form-icon"></i>East
          </label>
          <label class="form-radio form-inline">
            <input name="round" value="south" type="radio" />
            <i class="form-icon"></i>South
          </label>
          <label class="form-radio form-inline">
            <input name="round" value="west" type="radio" />
            <i class="form-icon"></i>West
          </label>
          <label class="form-radio form-inline">
            <input name="round" value="north" type="radio" />
            <i class="form-icon"></i>North
          </label>
        </div>
      </div>
      <div class="form-group">
        <div class="col-3 col-sm-12">
          <label class="form-label">Various</label>
        </div>
        <div class="col-9 col-sm-12">
          <label class="form-switch form-inline">
            <input type="checkbox" id="riichi" />
            <i class="form-icon"></i>Riichi
          </label>
          <label class="form-switch form-inline">
            <input type="checkbox" id="doubleriichi" />
            <i class="form-icon"></i>Double Riichi
          </label>
          <label class="form-switch form-inline">
            <input type="checkbox" id="ippatsu" />
            <i class="form-icon"></i>Ippatsu
          </label>
          <label class="form-switch form-inline">
            <input type="checkbox" id="haitei" />
            <i class="form-icon"></i>Haitei
          </label>
          <label class="form-switch form-inline">
            <input type="checkbox" id="rinshan" />
            <i class="form-icon"></i>Rinshan
          </label>
          <label class="form-switch form-inline">
            <input type="checkbox" id="chankan" />
            <i class="form-icon"></i>Chankan
          </label>
          <label class="form-switch form-inline">
            <input type="checkbox" id="tenhou" />
            <i class="form-icon"></i>Tenhou
          </label>
        </div>
      </div>
      <button class="btn btn-primary" onclick={ e: Event =>
            val tsumo = document.getElementsByName("tsumo").collect {
              case i: html.Input if i.checked => i.value
            }.head
            val seat = document.getElementsByName("seat").collect {
              case i: html.Input if i.checked => i.value
            }.head
            val round = document.getElementsByName("round").collect {
              case i: html.Input if i.checked => i.value
            }.head
            val special =
              Seq(
                riichi,
                doubleriichi,
                ippatsu,
                haitei,
                rinshan,
                chankan,
                tenhou).filter(_.checked).map(_.id)
            result.value.clear()
            result.value +=
              Adapter(
                hand.value,
                agaripai.value,
                aka.value,
                dora.value,
                tsumo,
                seat,
                round,
                special)
          }>
        Submit
      </button>
    </div>
    <div>
      <p>
      {
        for (r <- result) yield <h2 class="text-center">{ Adapter.resToStr(r) }</h2>
      }
      </p>
      <div class="columns">
        <div class="col-6 col-sm-12">
          <table class="table">
            <thead>
              <tr>
                <th>Yaku</th>
                <th>Han</th>
              </tr>
            </thead>
            <tbody>
            {
              Constants(result.bind.flatMap(_.yakus): _*).map { y =>
                <tr>
                  <td>{ y._1.toString }</td>
                  <td>{ y._2.toString }</td>
                </tr>
              }
            }
            </tbody>
          </table>
        </div>
        <div class="col-6 col-sm-12">
          <table class="table">
            <thead>
              <tr>
                <th>Fu</th>
                <th>Count</th>
              </tr>
            </thead>
            <tbody>
            {
              Constants(result.bind.flatMap(_.fus): _*).map { f =>
                <tr>
                  <td>{ f._1.toString }</td>
                  <td>{ f._2.toString }</td>
                </tr>
              }
            }
            </tbody>
          </table>
        </div>
      </div>
    </div>
  }
}

object Main {
  def main(args: Array[String]): Unit = {
    val ui = new Ui
    dom.render(document.getElementById("form"), ui.form)
  }
}
