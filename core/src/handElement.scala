package pw.nerde.riichala

sealed trait HandElement extends Ordered[HandElement] {
  def tile: Tile
  def tiles: Seq[Tile]
  def typeId: Int

  import scala.math.Ordered.orderingToOrdered
  import scala.math.Ordering.Implicits.seqDerivedOrdering
  def compare(that: HandElement): Int = {
    // (this.typeId, this.tiles) compare (that.typeId, that.tiles)
    if (this.typeId != that.typeId) this.typeId.compare(that.typeId)
    else this.tiles.compare(that.tiles)
  }
}

object HandElement {
  def from(tiles: Seq[Tile]): HandElement = tiles match {
    case Seq(a: Shuupai, b: Shuupai, c: Shuupai)
        if (a.i + 1 == b.i && b.i + 1 == c.i) =>
      Shuntsu(Seq(a, b, c))
    case Seq(_, _)       => Jantou(tiles)
    case Seq(_, _, _)    => Koutsu(tiles)
    case Seq(_, _, _, _) => Kantsu(tiles)
  }

  def isAgariKouOpen(
      tsumo: Boolean,
      closedKou: Seq[HandElement],
      closedAll: Seq[HandElement],
      agaripai: Tile): Boolean = {
    !tsumo &&
    (closedKou.find(_.tile == agaripai) match {
      case Some(k: KouKan) =>
        closedAll
          .flatMap(_.tiles)
          .count(_ == agaripai) == k.tiles.size
      case _ => false
    })
  }
}

sealed trait Mentsu extends HandElement

sealed trait KouKan extends Mentsu

case class Jantou(tiles: Seq[Tile]) extends HandElement {
  require(tiles.size == 2)

  def typeId: Int = 0
  def tile: Tile = tiles(0)
}

object Jantou {
  def apply(tile: Tile): Jantou = Jantou(Seq.fill(2)(tile))

  def unapply(j: Jantou): Option[Tile] = j.tiles.lift(0)
}

case class Shuntsu(tiles: Seq[Shuupai]) extends Mentsu {
  require(tiles.size == 3)

  def typeId: Int = 1
  def tile: Shuupai = tiles(0)
}

object Shuntsu {
  def apply(tile: Shuupai): Shuntsu =
    Shuntsu(Seq(tile, tile.incr, tile.incr.incr))

  def unapply(s: Shuntsu): Option[Shuupai] = s.tiles.lift(0)
}

case class Koutsu(tiles: Seq[Tile]) extends KouKan {
  require(tiles.size == 3)

  def typeId: Int = 2
  def tile: Tile = tiles(0)
}

object Koutsu {
  def apply(tile: Tile): Koutsu = Koutsu(Seq.fill(3)(tile))

  def unapply(s: Koutsu): Option[Tile] = s.tiles.lift(0)
}

case class Kantsu(tiles: Seq[Tile], fuuroAnkan: Boolean = false)
    extends KouKan {
  require(tiles.size == 4)

  def typeId: Int = 3
  def tile: Tile = tiles(0)
}

object Kantsu {
  def apply(tile: Tile): Kantsu = Kantsu(Seq.fill(4)(tile))

  def unapply(s: Kantsu): Option[Tile] = s.tiles.lift(0)
}
