package pw.nerde.riichala

object Yaku {
  sealed trait Yaku

  sealed trait TileYaku extends Yaku {
    def apply(
        ls: Seq[HandElement],
        winningHand: WinningHand,
        kaze: Seq[Kazehai]): Seq[(Yaku, Int)]
  }

  sealed trait SpecialYaku extends Yaku {
    def apply(): Seq[(Yaku, Int)]
    def yakuman: Boolean = false
  }

  sealed trait NormalYaku extends TileYaku

  sealed trait Yakuman extends TileYaku

  sealed trait DoraYaku extends Yaku

  object NormalYaku {
    val List: Seq[NormalYaku] = Seq(
      MenzenTsumo,
      Pinfu,
      Iipeikou,
      Tanyao,
      Yakuhai,
      Chantaiyao,
      SanshokuDonjun,
      Ittsu,
      Toitoi,
      Sanankou,
      SanshokuDonkou,
      Sankantsu,
      Chiitoitsu,
      Honroutou,
      Shousangen,
      Honitsu,
      JunchanTaiyao,
      Ryanpeikou,
      Chinitsu)
  }

  object SpecialYaku {
    val List: Seq[SpecialYaku] = Seq(
      Riichi,
      Ippatsu,
      Haitei,
      Houtei,
      Rinshan,
      Chankan,
      DoubleRiichi,
      Tenhou,
      Chiihou,
      Renhou)
  }

  object Yakuman {
    val List: Seq[Yakuman] = Seq(
      Suuankou,
      Daisangen,
      Shousuushii,
      Daisuushii,
      Tsuuiisou,
      Chinroutou,
      Ryuuiisou,
      ChuurenPoutou,
      Suukantsu)
  }

  case object MenzenTsumo extends NormalYaku {
    def apply(
        ls: Seq[HandElement],
        winningHand: WinningHand,
        kaze: Seq[Kazehai]): Seq[(Yaku, Int)] = {
      if (winningHand.tsumo && winningHand.menzen)
        Seq((MenzenTsumo, 1))
      else
        Seq.empty
    }
  }

  case object Pinfu extends NormalYaku {
    def apply(
        ls: Seq[HandElement],
        winningHand: WinningHand,
        kaze: Seq[Kazehai]): Seq[(Yaku, Int)] = {
      val allShun = ls.count(_.isInstanceOf[Shuntsu]) == 4
      val jantouFu = Fu.Jentou(ls, winningHand, kaze).nonEmpty
      val waitFu = Fu.Tenpai.forall(ls, winningHand, kaze) != 0
      if (winningHand.menzen && allShun && !jantouFu && !waitFu)
        Seq((Pinfu, 1))
      else
        Seq.empty
    }
  }

  case object Iipeikou extends NormalYaku {
    def apply(
        ls: Seq[HandElement],
        winningHand: WinningHand,
        kaze: Seq[Kazehai]): Seq[(Yaku, Int)] = {
      val dup =
        ls.collect { case m: Mentsu => m }
          .groupBy(identity)
          .map(_._2.size)
          .toSeq
          .sortBy(-_)
          .padTo(2, 0)
      if (winningHand.menzen && dup(0) >= 2 && dup(1) < 2)
        Seq((Iipeikou, 1))
      else
        Seq.empty
    }
  }

  case object Tanyao extends NormalYaku {
    def apply(
        ls: Seq[HandElement],
        winningHand: WinningHand,
        kaze: Seq[Kazehai]): Seq[(Yaku, Int)] = {
      if (!winningHand.allTiles.exists(t => Tile.isYaochuu(t)))
        Seq((Tanyao, 1))
      else
        Seq.empty
    }
  }

  case object Yakuhai extends NormalYaku {
    def apply(
        ls: Seq[HandElement],
        winningHand: WinningHand,
        kaze: Seq[Kazehai]): Seq[(Yaku, Int)] = {
      val cnt =
        ls.collect { case k: KouKan => k }
          .map(t => Tile.yakuhaiCount(t.tile, kaze))
          .sum
      if (cnt != 0) Seq((Yakuhai, cnt)) else Seq.empty
    }
  }

  case object Chantaiyao extends NormalYaku {
    def apply(
        ls: Seq[HandElement],
        winningHand: WinningHand,
        kaze: Seq[Kazehai]): Seq[(Yaku, Int)] = {
      val valid = ls.forall(_.tiles.exists(t => Tile.isYaochuu(t))) &&
        !ls.forall(_.tiles.exists(t => Tile.isTerminal(t))) &&
        ls.exists(_.isInstanceOf[Shuntsu])
      if (valid && winningHand.menzen)
        Seq((Chantaiyao, 2))
      else if (valid)
        Seq((Chantaiyao, 1))
      else
        Seq.empty
    }
  }

  case object SanshokuDonjun extends NormalYaku {
    def apply(
        ls: Seq[HandElement],
        winningHand: WinningHand,
        kaze: Seq[Kazehai]): Seq[(Yaku, Int)] = {
      val shun = ls.collect { case h: Shuntsu => h.tile }
      val valid =
        if (shun.isEmpty) false
        else {
          val maxShun = shun.groupBy(_.i).maxBy(_._2.size)._2
          maxShun.exists(_.isInstanceOf[Manzu]) &&
          maxShun.exists(_.isInstanceOf[Pinzu]) &&
          maxShun.exists(_.isInstanceOf[Souzu])
        }
      if (valid && winningHand.menzen)
        Seq((SanshokuDonjun, 2))
      else if (valid)
        Seq((SanshokuDonjun, 1))
      else
        Seq.empty
    }
  }

  case object Ittsu extends NormalYaku {
    def apply(
        ls: Seq[HandElement],
        winningHand: WinningHand,
        kaze: Seq[Kazehai]): Seq[(Yaku, Int)] = {
      val men = (1 to 9 by 3).map(i => Shuntsu(Manzu(i)))
      val pin = (1 to 9 by 3).map(i => Shuntsu(Pinzu(i)))
      val sou = (1 to 9 by 3).map(i => Shuntsu(Souzu(i)))
      val sort = ls.sorted
      val valid = Util.subtract(sort, men).isDefined ||
        Util.subtract(sort, pin).isDefined ||
        Util.subtract(sort, sou).isDefined
      if (valid && winningHand.menzen)
        Seq((Ittsu, 2))
      else if (valid)
        Seq((Ittsu, 1))
      else
        Seq.empty
    }
  }

  case object Toitoi extends NormalYaku {
    def apply(
        ls: Seq[HandElement],
        winningHand: WinningHand,
        kaze: Seq[Kazehai]): Seq[(Yaku, Int)] = {
      if (ls.collect { case k: KouKan => k }.size == 4)
        Seq((Toitoi, 2))
      else
        Seq.empty
    }
  }

  case object Sanankou extends NormalYaku {
    def apply(
        ls: Seq[HandElement],
        winningHand: WinningHand,
        kaze: Seq[Kazehai]): Seq[(Yaku, Int)] = {
      val hand = ls.collect { case k: KouKan                      => k }
      val fuuro = winningHand.hand.fuuro.collect { case k: KouKan => k }
      val (closedK, closedAll) =
        (hand.diff(fuuro), ls.diff(winningHand.hand.fuuro))
      val (tsumo, agari) = (winningHand.tsumo, winningHand.agaripai)
      val ofst =
        if (HandElement.isAgariKouOpen(tsumo, closedK, closedAll, agari))
          1
        else
          0
      val ankan = fuuro.collect { case k: Kantsu if k.fuuroAnkan => k }
      if (closedK.size + ankan.size - ofst == 3)
        Seq((Sanankou, 2))
      else
        Seq.empty
    }
  }

  case object SanshokuDonkou extends NormalYaku {
    def apply(
        ls: Seq[HandElement],
        winningHand: WinningHand,
        kaze: Seq[Kazehai]): Seq[(Yaku, Int)] = {
      val kou =
        ls.collect {
            case Koutsu(Seq(t: Shuupai, _*))    => t
            case Kantsu(Seq(t: Shuupai, _*), _) => t
          }
          .groupBy(_.i)
      val valid =
        if (kou.isEmpty) false
        else {
          val maxKou = kou.maxBy(_._2.size)._2
          maxKou.exists(_.isInstanceOf[Manzu]) &&
          maxKou.exists(_.isInstanceOf[Pinzu]) &&
          maxKou.exists(_.isInstanceOf[Souzu])
        }
      if (valid)
        Seq((SanshokuDonkou, 2))
      else
        Seq.empty
    }
  }

  case object Sankantsu extends NormalYaku {
    def apply(
        ls: Seq[HandElement],
        winningHand: WinningHand,
        kaze: Seq[Kazehai]): Seq[(Yaku, Int)] = {
      if (ls.count(_.isInstanceOf[Kantsu]) == 3)
        Seq((Sankantsu, 2))
      else
        Seq.empty
    }
  }

  case object Chiitoitsu extends NormalYaku {
    def apply(
        ls: Seq[HandElement],
        winningHand: WinningHand,
        kaze: Seq[Kazehai]): Seq[(Yaku, Int)] = {
      if (ls.distinct.count(_.isInstanceOf[Jantou]) == 7)
        Seq((Chiitoitsu, 2))
      else
        Seq.empty
    }
  }

  case object Honroutou extends NormalYaku {
    def apply(
        ls: Seq[HandElement],
        winningHand: WinningHand,
        kaze: Seq[Kazehai]): Seq[(Yaku, Int)] = {
      val allYao = winningHand.allTiles.forall(t => Tile.isYaochuu(t))
      if (allYao)
        Seq((Honroutou, 2))
      else
        Seq.empty
    }
  }

  case object Shousangen extends NormalYaku {
    def apply(
        ls: Seq[HandElement],
        winningHand: WinningHand,
        kaze: Seq[Kazehai]): Seq[(Yaku, Int)] = {
      val ji = ls.filter(_.tile.isInstanceOf[Sangenpai])
      if (ji.count(_.isInstanceOf[Jantou]) == 1 &&
          ji.count(_.isInstanceOf[KouKan]) == 2)
        Seq((Shousangen, 2))
      else
        Seq.empty
    }
  }

  case object Honitsu extends NormalYaku {
    def apply(
        ls: Seq[HandElement],
        winningHand: WinningHand,
        kaze: Seq[Kazehai]): Seq[(Yaku, Int)] = {
      val suit = ls.filter(_.tile.isInstanceOf[Shuupai])
      val valid = suit.size != ls.size &&
        suit.forall(_.tile.getClass == suit(0).tile.getClass)
      if (valid && winningHand.menzen)
        Seq((Honitsu, 3))
      else if (valid)
        Seq((Honitsu, 2))
      else
        Seq.empty
    }
  }

  case object JunchanTaiyao extends NormalYaku {
    def apply(
        ls: Seq[HandElement],
        winningHand: WinningHand,
        kaze: Seq[Kazehai]): Seq[(Yaku, Int)] = {
      val valid = ls.forall(_.tiles.exists(t => Tile.isTerminal(t)))
      if (valid && winningHand.menzen)
        Seq((JunchanTaiyao, 3))
      else if (valid)
        Seq((JunchanTaiyao, 2))
      else
        Seq.empty
    }
  }

  case object Ryanpeikou extends NormalYaku {
    def apply(
        ls: Seq[HandElement],
        winningHand: WinningHand,
        kaze: Seq[Kazehai]): Seq[(Yaku, Int)] = {
      val dup =
        ls.collect { case m: Mentsu => m }
          .groupBy(identity)
          .map(_._2.size)
          .toSeq
          .sortBy(-_)
          .padTo(2, 0)
      if (winningHand.menzen && dup(0) == 2 && dup(1) == 2)
        Seq((Ryanpeikou, 3))
      else
        Seq.empty
    }
  }

  case object Chinitsu extends NormalYaku {
    def apply(
        ls: Seq[HandElement],
        winningHand: WinningHand,
        kaze: Seq[Kazehai]): Seq[(Yaku, Int)] = {
      val suit = ls.filter(_.tile.isInstanceOf[Shuupai])
      val valid = suit.size == ls.size && suit.forall(
        _.tile.getClass == suit(0).tile.getClass)
      if (valid && winningHand.menzen)
        Seq((Chinitsu, 6))
      else if (valid)
        Seq((Chinitsu, 5))
      else
        Seq.empty
    }
  }

  case object Riichi extends SpecialYaku {
    def apply(): Seq[(Yaku, Int)] = Seq((Riichi, 1))
  }

  case object Ippatsu extends SpecialYaku {
    def apply(): Seq[(Yaku, Int)] = Seq((Ippatsu, 1))
  }

  case object Haitei extends SpecialYaku {
    def apply(): Seq[(Yaku, Int)] = Seq((Haitei, 1))
  }

  case object Houtei extends SpecialYaku {
    def apply(): Seq[(Yaku, Int)] = Seq((Houtei, 1))
  }

  case object Rinshan extends SpecialYaku {
    def apply(): Seq[(Yaku, Int)] = Seq((Rinshan, 1))
  }

  case object Chankan extends SpecialYaku {
    def apply(): Seq[(Yaku, Int)] = Seq((Chankan, 1))
  }

  case object DoubleRiichi extends SpecialYaku {
    def apply(): Seq[(Yaku, Int)] = Seq((DoubleRiichi, 2))
  }

  case object Tenhou extends SpecialYaku {
    def apply(): Seq[(Yaku, Int)] = Seq((Tenhou, 13))
    override val yakuman: Boolean = true
  }

  case object Chiihou extends SpecialYaku {
    def apply(): Seq[(Yaku, Int)] = Seq((Chiihou, 13))
    override val yakuman: Boolean = true
  }

  case object Renhou extends SpecialYaku {
    def apply(): Seq[(Yaku, Int)] = Seq((Renhou, 13))
    override val yakuman: Boolean = true
  }

  case object Suuankou extends Yakuman {
    def apply(
        ls: Seq[HandElement],
        winningHand: WinningHand,
        kaze: Seq[Kazehai]): Seq[(Yaku, Int)] = {
      val hand = ls.collect { case k: KouKan                      => k }
      val fuuro = winningHand.hand.fuuro.collect { case k: KouKan => k }
      val (closedK, closedAll) =
        (hand.diff(fuuro), ls.diff(winningHand.hand.fuuro))
      val (tsumo, agari) = (winningHand.tsumo, winningHand.agaripai)
      val ofst =
        if (HandElement.isAgariKouOpen(tsumo, closedK, closedAll, agari))
          1
        else
          0
      val ankan = fuuro.collect { case k: Kantsu if k.fuuroAnkan => k }
      if (closedK.size + ankan.size - ofst == 4)
        Seq((Suuankou, 13))
      else
        Seq.empty
    }
  }

  case object Daisangen extends Yakuman {
    def apply(
        ls: Seq[HandElement],
        winningHand: WinningHand,
        kaze: Seq[Kazehai]): Seq[(Yaku, Int)] = {
      val koukan = ls.collect { case k: KouKan => k }
      if (koukan.count(_.tile.isInstanceOf[Sangenpai]) == 3)
        Seq((Daisangen, 13))
      else
        Seq.empty
    }
  }

  case object Shousuushii extends Yakuman {
    def apply(
        ls: Seq[HandElement],
        winningHand: WinningHand,
        kaze: Seq[Kazehai]): Seq[(Yaku, Int)] = {
      val koukan = ls.collect { case k: KouKan => k }
      val jantou = ls.collect { case j: Jantou => j }
      val valid = koukan.count(_.tile.isInstanceOf[Kazehai]) == 3 &&
        jantou.count(_.tile.isInstanceOf[Kazehai]) == 1
      if (valid)
        Seq((Shousuushii, 13))
      else
        Seq.empty
    }
  }

  case object Daisuushii extends Yakuman {
    def apply(
        ls: Seq[HandElement],
        winningHand: WinningHand,
        kaze: Seq[Kazehai]): Seq[(Yaku, Int)] = {
      val koukan = ls.collect { case k: KouKan => k }
      val valid = koukan.count(_.tile.isInstanceOf[Kazehai]) == 4
      if (valid)
        Seq((Daisuushii, 13))
      else
        Seq.empty
    }
  }

  case object Tsuuiisou extends Yakuman {
    def apply(
        ls: Seq[HandElement],
        winningHand: WinningHand,
        kaze: Seq[Kazehai]): Seq[(Yaku, Int)] = {
      val allJi = ls.flatMap(_.tiles).forall(_.isInstanceOf[Jihai])
      if (allJi)
        Seq((Tsuuiisou, 13))
      else
        Seq.empty
    }
  }

  case object Chinroutou extends Yakuman {
    def apply(
        ls: Seq[HandElement],
        winningHand: WinningHand,
        kaze: Seq[Kazehai]): Seq[(Yaku, Int)] = {
      val allTerm = ls.flatMap(_.tiles).forall(t => Tile.isTerminal(t))
      if (allTerm)
        Seq((Chinroutou, 13))
      else
        Seq.empty
    }
  }

  case object Ryuuiisou extends Yakuman {
    def apply(
        ls: Seq[HandElement],
        winningHand: WinningHand,
        kaze: Seq[Kazehai]): Seq[(Yaku, Int)] = {
      val allGreen = ls.flatMap(_.tiles).forall(t => Tile.isGreen(t))
      if (allGreen)
        Seq((Ryuuiisou, 13))
      else
        Seq.empty
    }
  }

  case object ChuurenPoutou extends Yakuman {
    def apply(
        ls: Seq[HandElement],
        winningHand: WinningHand,
        kaze: Seq[Kazehai]): Seq[(Yaku, Int)] = {
      val tiles = ls.flatMap(_.tiles)
      val shuu = tiles.collect { case s: Shuupai => s }
      val sameSuit = shuu.filter(_.getClass == shuu(0).getClass)
      val numbers = sameSuit.map(_.i)
      val pattern = Seq(1, 1, 1, 2, 3, 4, 5, 6, 7, 8, 9, 9, 9)
      if (winningHand.menzen &&
          numbers.size == 14 &&
          Util.subtract(numbers, pattern).isDefined)
        Seq((ChuurenPoutou, 13))
      else
        Seq.empty
    }
  }

  case object Suukantsu extends Yakuman {
    def apply(
        ls: Seq[HandElement],
        winningHand: WinningHand,
        kaze: Seq[Kazehai]): Seq[(Yaku, Int)] = {
      val kanCnt = ls.count(_.isInstanceOf[Kantsu])
      if (kanCnt == 4)
        Seq((Suukantsu, 13))
      else
        Seq.empty
    }
  }

  case object KukushiMusou extends Yaku {
    def apply(winningHand: WinningHand): Seq[(Yaku, Int)] = {
      val allYao = winningHand.allTiles.forall(t => Tile.isYaochuu(t))
      val distinct = winningHand.allTiles.distinct.size
      if (allYao && distinct == 13)
        Seq((KukushiMusou, 13))
      else
        Seq.empty
    }
  }

  case object AkaDora extends DoraYaku

  case object Dora extends DoraYaku
}
