package pw.nerde.riichala

object Fu {
  sealed trait Fu

  sealed trait NormalFu extends Fu {
    def apply(
        ls: Seq[HandElement],
        winningHand: WinningHand,
        kaze: Seq[Kazehai]): Seq[(Fu, Int)]
  }

  sealed trait SpecialFu extends Fu

  object NormalFu {
    val List: Seq[NormalFu] = Seq(
      Base,
      Menzen,
      Tsumo,
      Tenpai,
      Jentou,
      Minkou,
      MinkouYaochuu,
      Ankou,
      AnkouYaochuu,
      Minkan,
      MinkanYaochuu,
      Ankan,
      AnkanYaochuu)
  }

  case object Base extends NormalFu {
    def apply(
        ls: Seq[HandElement],
        winningHand: WinningHand,
        kaze: Seq[Kazehai]): Seq[(Fu, Int)] = Seq((Base, 20))
  }

  case object Menzen extends NormalFu {
    def apply(
        ls: Seq[HandElement],
        winningHand: WinningHand,
        kaze: Seq[Kazehai]): Seq[(Fu, Int)] = {
      val chii = Yaku.Chiitoitsu(ls, winningHand, kaze)
      if (winningHand.menzen && !winningHand.tsumo && chii.isEmpty)
        Seq((Menzen, 10))
      else
        Seq.empty
    }
  }

  case object Tsumo extends NormalFu {
    def apply(
        ls: Seq[HandElement],
        winningHand: WinningHand,
        kaze: Seq[Kazehai]): Seq[(Fu, Int)] = {
      if (winningHand.tsumo)
        Seq((Tsumo, 2))
      else
        Seq.empty
    }
  }

  case object Tenpai extends NormalFu {
    def forall(
        ls: Seq[HandElement],
        winningHand: WinningHand,
        kaze: Seq[Kazehai]): Int = {
      val closed = ls.diff(winningHand.hand.fuuro)
      val jantou =
        closed.exists(h =>
          h.isInstanceOf[Jantou] && h.tile == winningHand.agaripai) &&
          closed.flatMap(_.tiles).count(_ == winningHand.agaripai) == 2
      val shun = winningHand.agaripai match {
        case ap: Shuupai =>
          val seq =
            closed.collect { case h: Shuntsu => h }.filter(_.tiles.contains(ap))
          seq.nonEmpty && seq.forall { h =>
            val ht = h.tiles
            val kanchan = ht(1) == ap
            val penchan = (ht(0).i, ap.i) == (1, 3) || (ap.i, ht(2).i) == (7, 9)
            kanchan || penchan
          }
        case _ => false
      }
      if (jantou || shun)
        2
      else
        0
    }

    def exists(
        ls: Seq[HandElement],
        winningHand: WinningHand,
        kaze: Seq[Kazehai]): Int = {
      val closed = ls.diff(winningHand.hand.fuuro)
      val jantou =
        closed.exists(h =>
          h.isInstanceOf[Jantou] && h.tile == winningHand.agaripai)
      val shun = winningHand.agaripai match {
        case ap: Shuupai =>
          val seq =
            closed.collect { case h: Shuntsu => h }.filter(_.tiles.contains(ap))
          seq.nonEmpty && seq.exists { h =>
            val ht = h.tiles
            val kanchan = ht(1) == ap
            val penchan = (ht(0).i, ap.i) == (1, 3) || (ap.i, ht(2).i) == (7, 9)
            kanchan || penchan
          }
        case _ => false
      }
      if (jantou || shun)
        2
      else
        0
    }

    def apply(
        ls: Seq[HandElement],
        winningHand: WinningHand,
        kaze: Seq[Kazehai]): Seq[(Fu, Int)] = {
      if (Yaku.Pinfu(ls, winningHand, kaze).nonEmpty)
        Seq.empty
      else
        Seq((Tenpai, exists(ls, winningHand, kaze)))
    }
  }

  case object Jentou extends NormalFu {
    def apply(
        ls: Seq[HandElement],
        winningHand: WinningHand,
        kaze: Seq[Kazehai]): Seq[(Fu, Int)] = {
      val jentouYaku =
        ls.find(_.isInstanceOf[Jantou])
          .map(t => Tile.yakuhaiCount(t.tile, kaze))
          .getOrElse(0)
      if (jentouYaku != 0)
        Seq((Jentou, jentouYaku * 2))
      else
        Seq.empty
    }
  }

  case object Minkou extends NormalFu {
    def apply(
        ls: Seq[HandElement],
        winningHand: WinningHand,
        kaze: Seq[Kazehai]): Seq[(Fu, Int)] = {
      val hand =
        ls.filter(h => h.isInstanceOf[Koutsu] && !Tile.isYaochuu(h.tile))
      val fuuro = winningHand.hand.fuuro.filter(h =>
        h.isInstanceOf[Koutsu] && !Tile.isYaochuu(h.tile))
      val (closedK, closedAll) =
        (hand.diff(fuuro), ls.diff(winningHand.hand.fuuro))
      val (tsumo, agari) = (winningHand.tsumo, winningHand.agaripai)
      val ofst =
        if (HandElement.isAgariKouOpen(tsumo, closedK, closedAll, agari))
          1
        else
          0
      if (fuuro.size + ofst != 0)
        Seq((Minkou, (fuuro.size + ofst) * 2))
      else
        Seq.empty
    }
  }

  case object MinkouYaochuu extends NormalFu {
    def apply(
        ls: Seq[HandElement],
        winningHand: WinningHand,
        kaze: Seq[Kazehai]): Seq[(Fu, Int)] = {
      val hand =
        ls.filter(h => h.isInstanceOf[Koutsu] && Tile.isYaochuu(h.tile))
      val fuuro = winningHand.hand.fuuro.filter(h =>
        h.isInstanceOf[Koutsu] && Tile.isYaochuu(h.tile))
      val (closedK, closedAll) =
        (hand.diff(fuuro), ls.diff(winningHand.hand.fuuro))
      val (tsumo, agari) = (winningHand.tsumo, winningHand.agaripai)
      val ofst =
        if (HandElement.isAgariKouOpen(tsumo, closedK, closedAll, agari))
          1
        else
          0
      if (fuuro.size + ofst != 0)
        Seq((MinkouYaochuu, (fuuro.size + ofst) * 4))
      else
        Seq.empty
    }
  }

  case object Ankou extends NormalFu {
    def apply(
        ls: Seq[HandElement],
        winningHand: WinningHand,
        kaze: Seq[Kazehai]): Seq[(Fu, Int)] = {
      val hand =
        ls.filter(h => h.isInstanceOf[Koutsu] && !Tile.isYaochuu(h.tile))
      val fuuro = winningHand.hand.fuuro.filter(h =>
        h.isInstanceOf[Koutsu] && !Tile.isYaochuu(h.tile))
      val (closedK, closedAll) =
        (hand.diff(fuuro), ls.diff(winningHand.hand.fuuro))
      val (tsumo, agari) = (winningHand.tsumo, winningHand.agaripai)
      val ofst =
        if (HandElement.isAgariKouOpen(tsumo, closedK, closedAll, agari))
          1
        else
          0
      if (closedK.size - ofst != 0)
        Seq((Ankou, (closedK.size - ofst) * 4))
      else
        Seq.empty
    }
  }

  case object AnkouYaochuu extends NormalFu {
    def apply(
        ls: Seq[HandElement],
        winningHand: WinningHand,
        kaze: Seq[Kazehai]): Seq[(Fu, Int)] = {
      val hand =
        ls.filter(h => h.isInstanceOf[Koutsu] && Tile.isYaochuu(h.tile))
      val fuuro = winningHand.hand.fuuro.filter(h =>
        h.isInstanceOf[Koutsu] && Tile.isYaochuu(h.tile))
      val (closedK, closedAll) =
        (hand.diff(fuuro), ls.diff(winningHand.hand.fuuro))
      val (tsumo, agari) = (winningHand.tsumo, winningHand.agaripai)
      val ofst =
        if (HandElement.isAgariKouOpen(tsumo, closedK, closedAll, agari))
          1
        else
          0
      if (closedK.size - ofst != 0)
        Seq((AnkouYaochuu, (closedK.size - ofst) * 8))
      else
        Seq.empty
    }
  }

  case object Minkan extends NormalFu {
    def apply(
        ls: Seq[HandElement],
        winningHand: WinningHand,
        kaze: Seq[Kazehai]): Seq[(Fu, Int)] = {
      val hand =
        ls.filter(h => h.isInstanceOf[Kantsu] && !Tile.isYaochuu(h.tile))
      val fuuro =
        winningHand.hand.fuuro
          .collect { case k: Kantsu => k }
          .filter(h => !Tile.isYaochuu(h.tile) && !h.fuuroAnkan)
      val ofst =
        if (!winningHand.tsumo && hand.exists(_.tile == winningHand.agaripai))
          1
        else
          0
      if (fuuro.size + ofst != 0)
        Seq((Minkan, (fuuro.size + ofst) * 8))
      else
        Seq.empty
    }
  }

  case object MinkanYaochuu extends NormalFu {
    def apply(
        ls: Seq[HandElement],
        winningHand: WinningHand,
        kaze: Seq[Kazehai]): Seq[(Fu, Int)] = {
      val hand =
        ls.filter(h => h.isInstanceOf[Kantsu] && Tile.isYaochuu(h.tile))
      val fuuro =
        winningHand.hand.fuuro
          .collect { case k: Kantsu => k }
          .filter(h => Tile.isYaochuu(h.tile) && !h.fuuroAnkan)
      val ofst =
        if (!winningHand.tsumo && hand.exists(_.tile == winningHand.agaripai))
          1
        else
          0
      if (fuuro.size + ofst != 0)
        Seq((MinkanYaochuu, (fuuro.size + ofst) * 16))
      else
        Seq.empty
    }
  }

  case object Ankan extends NormalFu {
    def apply(
        ls: Seq[HandElement],
        winningHand: WinningHand,
        kaze: Seq[Kazehai]): Seq[(Fu, Int)] = {
      val hand =
        ls.filter(h => h.isInstanceOf[Kantsu] && !Tile.isYaochuu(h.tile))
      val fuuro =
        winningHand.hand.fuuro
          .collect { case k: Kantsu => k }
          .filter(h => !Tile.isYaochuu(h.tile) && !h.fuuroAnkan)
      val ofst =
        if (!winningHand.tsumo && hand.exists(_.tile == winningHand.agaripai))
          1
        else
          0
      val value = hand.size - (fuuro.size + ofst)
      if (value != 0)
        Seq((Ankan, value * 16))
      else
        Seq.empty
    }
  }

  case object AnkanYaochuu extends NormalFu {
    def apply(
        ls: Seq[HandElement],
        winningHand: WinningHand,
        kaze: Seq[Kazehai]): Seq[(Fu, Int)] = {
      val hand =
        ls.filter(h => h.isInstanceOf[Kantsu] && Tile.isYaochuu(h.tile))
      val fuuro =
        winningHand.hand.fuuro
          .collect { case k: Kantsu => k }
          .filter(h => Tile.isYaochuu(h.tile) && !h.fuuroAnkan)
      val ofst =
        if (!winningHand.tsumo && hand.exists(_.tile == winningHand.agaripai))
          1
        else
          0
      val value = hand.size - (fuuro.size + ofst)
      if (value != 0)
        Seq((AnkanYaochuu, value * 32))
      else
        Seq.empty
    }
  }

  case object Chiitoitsu extends SpecialFu

  case object PinfuTsumo extends SpecialFu

  case object OpenPinfu extends SpecialFu

  case object Round extends SpecialFu
}
