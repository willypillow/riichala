package pw.nerde.riichala

case class Hand(tiles: Seq[Tile], fuuro: Seq[Mentsu])

case class WinningHand(
    hand: Hand,
    tsumo: Boolean,
    oya: Boolean,
    agaripai: Tile,
    specialYaku: Seq[Yaku.SpecialYaku],
    akaCount: Int) {
  def menzen: Boolean = hand.fuuro.forall {
    case k: Kantsu => k.fuuroAnkan
    case _         => false
  }

  def allTiles: Seq[Tile] = hand.tiles ++ hand.fuuro.flatMap(_.tiles)
}
