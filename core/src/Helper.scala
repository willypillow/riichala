/*
case class Helper(ls: Seq[HandElement], winningHand: WinningHand, kaze: Seq[Kaze]) {
  lazy val pinfu: Seq[(Yaku, Int)] =
    Yaku.Pinfu.eval(ls, winningHand, kaze)

  lazy val agariKouOpen: Boolean = {
    val (tsumo, agari) = (winningHand.tsumo, winningHand.agaripai)
    val closedAll = ls.diff(winningHand.hand.fuuro)
    val closedKou = closedAll.collect { case k: KouKan => k }
    !tsumo &&
      (closedKou.find(_.tile == agari) match { case Some(k: KouKan) =>
          closedAll
            .flatMap(_.tiles)
            .count(_ == agaripai) == k.tiles.size
        case _ => false
      })
  }

  lazy val shun = ls collect { case s: Shuntsu => s }
}
 */
