package pw.nerde.riichala

object Util {
  def subtract[A](
      a: Seq[A],
      b: Seq[A],
      accRemain: List[A] = List.empty,
      accTake: List[A] = List.empty): Option[(Seq[A], Seq[A])] = {
    (a, b) match {
      case (Seq(), Seq(bb, bs @ _*)) => None
      case (Seq(aa, as @ _*), Seq(bb, bs @ _*)) if aa == bb =>
        subtract(as, bs, accRemain, aa :: accTake)
      case (Seq(aa, as @ _*), _) =>
        subtract(as, b, aa :: accRemain, accTake)
      case (_, _) => Option((accRemain.reverse, accTake.reverse))
    }
  }

  def ceil10(x: Int): Int = (x / 10.0).ceil.toInt * 10

  def ceil100(x: Int): Int = (x / 100.0).ceil.toInt * 100
}
