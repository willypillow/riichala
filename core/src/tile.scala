package pw.nerde.riichala

sealed trait Tile extends Ordered[Tile] {
  def incr: Tile
  def id: Int

  import scala.math.Ordered.orderingToOrdered
  def compare(that: Tile): Int = this.id.compare(that.id)
}

sealed trait Shuupai extends Tile {
  def i: Int
  def incr: Shuupai
}

sealed trait Jihai extends Tile

sealed trait Kazehai extends Jihai

sealed trait Sangenpai extends Jihai

case class Manzu(i: Int) extends Shuupai {
  def incr = this.copy(i = this.i % 9 + 1)
  def id: Int = 0 + i * 4
}

case class Pinzu(i: Int) extends Shuupai {
  def incr = this.copy(i = this.i % 9 + 1)
  def id: Int = 36 + i * 4
}

case class Souzu(i: Int) extends Shuupai {
  def incr = this.copy(i = this.i % 9 + 1)
  def id: Int = 72 + i * 4
}

case object Ton extends Kazehai {
  def incr = Nan
  def id: Int = 108 + 0 * 4
}

case object Nan extends Kazehai {
  def incr = Sha
  def id: Int = 108 + 1 * 4
}

case object Sha extends Kazehai {
  def incr = Pei
  def id: Int = 108 + 2 * 4
}

case object Pei extends Kazehai {
  def incr = Ton
  def id: Int = 108 + 3 * 4
}

case object Haku extends Sangenpai {
  def incr = Hatsu
  def id: Int = 124 + 0 * 4
}

case object Hatsu extends Sangenpai {
  def incr = Chun
  def id: Int = 124 + 1 * 4
}

case object Chun extends Sangenpai {
  def incr = Haku
  def id: Int = 124 + 2 * 4
}

object Tile {
  def isYaochuu(t: Tile): Boolean = t match {
    case tt: Shuupai => tt.i == 1 || tt.i == 9
    case _: Jihai    => true
    case _           => false
  }

  def isTerminal(t: Tile): Boolean = t match {
    case tt: Shuupai => tt.i == 1 || tt.i == 9
    case _           => false
  }

  def isGreen(t: Tile): Boolean = t match {
    case Hatsu                                          => true
    case tt: Souzu if Seq(2, 3, 4, 6, 8).contains(tt.i) => true
    case _                                              => false
  }

  def yakuhaiCount(t: Tile, kaze: Seq[Kazehai]): Int = t match {
    case tt: Sangenpai => 1
    case tt: Kazehai   => kaze.count(_ == tt)
    case _             => 0
  }
}
