package pw.nerde.riichala

object ScoreCalculator {
  sealed trait Cost {
    def total: Int
  }

  case object NoAgari extends Cost {
    def total: Int = 0
  }

  case class OyaTsumo(cost: Int) extends Cost {
    def total: Int = cost * 3
  }

  case class Tsumo(cost: Int, oyaCost: Int) extends Cost {
    def total: Int = oyaCost + cost * 2
  }

  case class Ron(cost: Int) extends Cost {
    def total: Int = cost
  }

  def costOf(score: Int, tsumo: Boolean, oya: Boolean): Cost = {
    if (tsumo) {
      if (oya) OyaTsumo(Util.ceil100(score * 2))
      else Tsumo(Util.ceil100(score), Util.ceil100(score * 2))
    } else {
      if (oya) Ron(Util.ceil100(score * 6))
      else Ron(Util.ceil100(score * 4))
    }
  }

  case class Result(
      baseScore: Int,
      limit: Limit,
      cost: Cost,
      yakus: Seq[(Yaku.Yaku, Int)],
      fus: Seq[(Fu.Fu, Int)])

  def partition(
      tiles: Seq[Tile],
      result: List[HandElement] = List.empty): Seq[List[HandElement]] =
    tiles match {
      case Seq(t, ts @ _*) =>
        val types = t match {
          case tt: Shuupai => Seq(Jantou(tt), Koutsu(tt), Shuntsu(tt))
          case tt          => Seq(Jantou(tt), Koutsu(tt))
        }
        types.flatMap { h =>
          Util.subtract(tiles, h.tiles).map {
            case (remain, meld) =>
              partition(remain, HandElement.from(meld) :: result)
          }
        }.flatten
      case _ =>
        if (Seq(1, 7).contains(result.count(_.isInstanceOf[Jantou])))
          Seq(result.reverse)
        else
          Seq()
    }

  def baseScore(
      ls: Seq[HandElement],
      winningHand: WinningHand,
      kaze: Seq[Kazehai],
      yakuOffset: Seq[(Yaku.Yaku, Int)])
    : (Int, Limit, Seq[(Yaku.Yaku, Int)], Seq[(Fu.Fu, Int)]) = {
    var yakus = hanOf(ls, winningHand, kaze)
    if (!yakus.exists(_._1.isInstanceOf[Yaku.Yakuman])) yakus ++= yakuOffset
    val fus = fuOf(ls, winningHand, kaze, yakus)
    val (yakuCnt, fuCnt) = (yakus.map(_._2).sum, fus.map(_._2).sum)
    val (score, limit) = yakuCnt match {
      case i if i <= 4 && (fuCnt << (i + 2)) <= 2000 =>
        (fuCnt << (i + 2), NoLimit)
      case i if i <= 5              => (2000, Mangan)
      case i if 6 to 7 contains i   => (3000, Haneman)
      case i if 8 to 10 contains i  => (4000, Baiman)
      case i if 11 to 12 contains i => (6000, Sanbaiman)
      case i if i >= 13             => (8000, Yakuman)
    }
    if (limit == Yakuman && (yakus ++ yakuOffset).exists(_._1.isInstanceOf[Yaku.Yakuman])) {
      val yakumans =
        yakus.filter(_._1.isInstanceOf[Yaku.Yakuman]) ++
        yakuOffset.filter(y => y._1.isInstanceOf[Yaku.SpecialYaku] && y._1.asInstanceOf[Yaku.SpecialYaku].yakuman)
      (8000 * (yakumans.map(_._2).sum / 13), Yakuman, yakumans, fus)
    } else {
      (score, limit, yakus, fus)
    }
  }

  def hanOf(
      ls: Seq[HandElement],
      winningHand: WinningHand,
      kaze: Seq[Kazehai]): Seq[(Yaku.Yaku, Int)] = {
    val yakuman = Yaku.Yakuman.List.flatMap(_(ls, winningHand, kaze))
    if (yakuman.nonEmpty)
      yakuman
    else
      Yaku.NormalYaku.List.flatMap(_(ls, winningHand, kaze))
  }

  def fuOf(
      ls: Seq[HandElement],
      winningHand: WinningHand,
      kaze: Seq[Kazehai],
      yakus: Seq[(Yaku.Yaku, Int)]): Seq[(Fu.Fu, Int)] = {
    if (yakus.exists(_._1 == Yaku.Chiitoitsu)) {
      Seq((Fu.Chiitoitsu, 25))
    } else if (yakus.exists(_._1 == Yaku.Pinfu) && winningHand.tsumo) {
      Seq((Fu.PinfuTsumo, 20))
    } else {
      val res = Fu.NormalFu.List.flatMap(_(ls, winningHand, kaze))
      val sum = res.map(_._2).sum
      if (sum == 20) {
        Seq((Fu.OpenPinfu, 30))
      } else {
        val ceil = Util.ceil10(sum)
        if (sum != ceil) res ++ Seq((Fu.Round, ceil - sum)) else res
      }
    }
  }

  def apply(
      winningHand: WinningHand,
      kaze: Seq[Kazehai],
      doras: Seq[Tile]): Result = {
    val special = winningHand.specialYaku
    val kukushi = Yaku.KukushiMusou(winningHand)
    if (kukushi.nonEmpty) {
      val specYakuman = special.filter(_.yakuman).flatMap(_())
      val score = 8000 * ((specYakuman.map(_._2).sum + kukushi.map(_._2).sum) / 13)
      val cost = costOf(score, winningHand.tsumo, winningHand.oya)
      Result(score, Yakuman, cost, specYakuman ++ kukushi, Seq.empty)
    } else {
      val specHan = special.flatMap(_())
      val akaHan =
        if (winningHand.akaCount != 0)
          Seq((Yaku.AkaDora, winningHand.akaCount))
        else
          Seq()
      val doraCnt = winningHand.allTiles
        .map(t => doras.count(t == _.incr))
        .sum
      val doraHan = if (doraCnt != 0) Seq((Yaku.Dora, doraCnt)) else Seq()
      val perms = partition(winningHand.hand.tiles)
        .filter { p =>
          val total = p ++ winningHand.hand.fuuro
          val janCnt = total.count(_.isInstanceOf[Jantou])
          val menCnt = total.count(_.isInstanceOf[Mentsu])
          (janCnt, menCnt) == (1, 4) || (janCnt, menCnt) == (7, 0)
        }
        .map { p =>
          baseScore(
            p ++ winningHand.hand.fuuro,
            winningHand,
            kaze,
            specHan ++ akaHan ++ doraHan)
        }
      if (perms.isEmpty) {
        Result(0, NoLimit, NoAgari, Seq.empty, Seq.empty)
      } else {
        val (score, limit, yakus, fus): (
            Int,
            Limit,
            Seq[(Yaku.Yaku, Int)],
            Seq[(Fu.Fu, Int)]) =
          perms.maxBy(t => (t._1, t._3.size))
        if (yakus.count(!_._1.isInstanceOf[Yaku.DoraYaku]) == 0) {
          Result(0, NoLimit, NoAgari, Seq.empty, Seq.empty)
        } else {
          val cost = costOf(score, winningHand.tsumo, winningHand.oya)
          Result(score, limit, cost, yakus, fus)
        }
      }
    }
  }
}
