package pw.nerde.riichala

trait Limit

case object NoLimit extends Limit

case object Mangan extends Limit

case object Haneman extends Limit

case object Baiman extends Limit

case object Sanbaiman extends Limit

case object Yakuman extends Limit
