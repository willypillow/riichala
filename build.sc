import mill._, scalalib._, scalajslib._, scalafmt._

def scalaVer = "2.12.8"
def scalaJSVer = "0.6.26"

object coreJvm extends ScalaModule with ScalafmtModule {
	def scalaVersion = scalaVer
	def scalacOptions = Seq("-deprecation", "-Ybackend-parallelism", "4")
	def millSourcePath = build.millSourcePath / "core"
}

object coreJs extends ScalaJSModule {
	def scalaVersion = scalaVer
	def scalaJSVersion = scalaJSVer
	def scalacOptions = Seq("-deprecation", "-Ybackend-parallelism", "4")
	def millSourcePath = build.millSourcePath / "core"
}

object test extends ScalaModule with ScalafmtModule {
	def scalaVersion = scalaVer
	def scalacOptions = Seq("-deprecation", "-Ybackend-parallelism", "4")
	def moduleDeps = Seq(coreJvm)
	def ivyDeps = Agg(ivy"org.scala-lang.modules::scala-xml:1.1.0")
}

object jsapp extends ScalaJSModule with ScalafmtModule {
	def scalaVersion = scalaVer
	def scalaJSVersion = scalaJSVer
	def scalacOptions = Seq("-deprecation", "-Ybackend-parallelism", "4")
	def moduleDeps = Seq(coreJs)
	def ivyDeps = Agg(
		ivy"com.thoughtworks.binding::dom_sjs0.6:11.2.0",
		ivy"com.thoughtworks.binding::binding_sjs0.6:11.2.0",
		ivy"org.scala-js::scalajs-dom_sjs0.6:0.9.2")
	def scalacPluginIvyDeps =
		super.scalacPluginIvyDeps() ++
		Agg(ivy"org.scalamacros:::paradise:2.1.0")
}
