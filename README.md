# Riichala #
A Riichi mahjong score calculator library in Scala.

## Structure ##
- `core/`: Core library.
- `test/`: Test that reads [Tenhou log](https://github.com/MahjongRepository/phoenix-logs) files to verify the correctness of the library.
- `jsapp/`: Sample web application that utilizes the Scala.js version of the library.

## Usage ##
```
$ mill -i coreJvm.repl

@ import pw.nerde.riichala._
import pw.nerde.riichala._

@ val hand = Hand(Seq(Manzu(2), Manzu(2), Manzu(2), Manzu(3), Manzu(3), Manzu(3), Manzu(4), Manzu(4), Manzu(4), Manzu(5), Manzu(5)), Seq(Koutsu(Manzu(1))))
hand: Hand = Hand(
  List(
    Manzu(2),
    Manzu(2),
    Manzu(2),
    Manzu(3),
    Manzu(3),
    Manzu(3),
    Manzu(4),
    Manzu(4),
    Manzu(4),
    Manzu(5),
    Manzu(5)
  ),
  List(Koutsu(List(Manzu(1), Manzu(1), Manzu(1))))
)

@ val wHand = WinningHand(hand, tsumo=true, oya=true, agaripai=Manzu(4), specialYaku=Seq.empty, akaCount=0)
wHand: WinningHand = WinningHand(
  Hand(
    List(
      Manzu(2),
      Manzu(2),
      Manzu(2),
      Manzu(3),
      Manzu(3),
      Manzu(3),
      Manzu(4),
      Manzu(4),
      Manzu(4),
      Manzu(5),
      Manzu(5)
    ),
    List(Koutsu(List(Manzu(1), Manzu(1), Manzu(1))))
  ),
  true,
  true,
  Manzu(4),
  List(),
  0
)

@ ScoreCalculator(wHand, kaze=Seq(Ton), doras=Seq(Manzu(1)))
res7: ScoreCalculator.Result = Result(
  6000,
  Sanbaiman,
  OyaTsumo(12000),
  List((Toitoi, 2), (Sanankou, 2), (Chinitsu, 5), (Dora, 3)),
  List(
    (Base, 20),
    (Tsumo, 2),
    (Tenpai, 0),
    (MinkouYaochuu, 4),
    (Ankou, 12),
    (Round, 2)
  )
)
```

## Known Issues / Todo ##
- Add instructions on how to download logs from Tenhou
- Documentation
- Fix deprecation warning
- Various refactoring
- Translation of jsapp
- Display the combination of tiles in jsapp
- Backport jsapp features from [mahjongreader](https://gitlab.com/WillyPillow/mahjongreader)
- Add a config class non-Tenhou rules
- Performance optimization
- Cross build for Scala Native
